# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EBUILD="6"
DESCRIPTION="Fake ebuild to mask pycrypto in a overlay"
HOMEPAGE="https://gitlab.com/lanodan/overlay/issues/6 http://www.dlitz.net/software/pycrypto/ https://pypi.python.org/pypi/pycrypto"
LICENSE="public-domain"
KEYWORDS="alpha amd64 arm ~arm64 hppa ia64 ~m68k ~mips ppc ppc64 ~s390 ~sh sparc x86 ~ppc-aix ~amd64-fbsd ~sparc-fbsd ~x86-fbsd ~amd64-linux ~arm-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~m68k-mint ~sparc-solaris ~x86-solaris"
SLOT="0"
