# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: python-whatever
# @MAINTAINER: Haelwenn (lanodan) Monnier <contact@hacktivis.me>
# @AUTHOR: Haelwenn (lanodan) Monnier <contact@hacktivis.me>
# @BLURB: For packages needing whatever python version

PYTHON_COMPAT=(${PYTHON_TARGETS}) # Hacky Hacky
