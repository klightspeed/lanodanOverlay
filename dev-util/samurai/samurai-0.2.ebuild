# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="ninja-compatible build tool written in C"
HOMEPAGE="https://github.com/michaelforney/samurai"
SRC_URI="https://github.com/michaelforney/samurai/archive/0.2.tar.gz -> ${P}.tar.gz"
LICENSE="ISC Apache-2.0 MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"
